#include <ros/ros.h>
#include <srvs/IntSum.h>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "client", ros::init_options::AnonymousName);
    ros::NodeHandle n;
    ros::service::waitForService("service", -1);
    ros::ServiceClient c = n.serviceClient<srvs::IntSum>("service");
    srvs::IntSum srv;
    char buffer1[33];
    char buffer2[33];
    char buffer3[33];
    for (int i = 0; i <= 1; ++i)
    {
        srv.request.first = i;
	srv.request.second = i + 5;
		
        snprintf(buffer1, sizeof(buffer1), "%d", srv.request.first);	
        snprintf(buffer2, sizeof(buffer2), "%d", srv.request.second);	
        ROS_INFO_STREAM("Calling service: bool data = " << buffer1 << " + "<< buffer2 << "...");

        if (c.call(srv))
        {
            snprintf(buffer3, sizeof(buffer3), "%d", srv.response.result);	
            ROS_INFO_STREAM("sum success: " << buffer3);
        }
        else
        {
            ROS_ERROR("Service call failed");
        }
    }
    return 0;
}


