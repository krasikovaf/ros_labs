#include <ros/ros.h>
#include <srvs/IntSum.h>

bool callback(srvs::IntSum::Request &req, srvs::IntSum::Response &resp)
{
    resp.result = req.first + req.second;
    return true;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "server");
    ros::NodeHandle n;
    ros::ServiceServer s = n.advertiseService("service", callback);
    ros::spin();
    return 0;
}


