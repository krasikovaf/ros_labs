#!/usr/bin/env python
import rospy
from srvs.srv import IntSum, IntSumResponse

def client(first, second):
    rospy.wait_for_service("service")
    try:
        sp = rospy.ServiceProxy("service", IntSum)
        resp = sp(first, second)
        print "Result is %d" % resp.result
    except rospy.ServiceException, e:
        print "Service call failed: " + str(e)

if __name__ == "__main__":
    print "Calling service: 3 + 5 ="
    client(3, 5)
    print "Calling service: 100 + 22 ="
    client(100, 22)


