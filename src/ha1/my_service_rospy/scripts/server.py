#!/usr/bin/env python
import rospy
from srvs.srv import IntSum, IntSumResponse

def callback(req):
    rospy.loginfo("someone accessed me...\n")
    return IntSumResponse(req.first + req.second)

def server():
    rospy.init_node("server")
    s = rospy.Service("service", IntSum, callback)
    rospy.spin()

if __name__ == "__main__":
    server()

