#include <ros/ros.h>
#include <stdlib.h>
#include <time.h>
#include <msgs/Match.h>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "publisher");
    ros::NodeHandle n;
    ros::Publisher p = n.advertise<msgs::Match>("topic", 5);
    ros::Rate rate(0.25);
    while (ros::ok())
    {
	srand(time(0));
        msgs::Match msg;
        msg.stamp = ros::Time::now();
        msg.team_one = rand() % 100;
        msg.team_two = rand() % 100;
	
        msg.comment = "what a goal!";

        p.publish(msg);
        rate.sleep();
    }
    return 0;
}


