#include <ros/ros.h>
#include <stdio.h>
#include <time.h>
#include <msgs/Match.h>

void callback(const msgs::Match::ConstPtr& msg)
{
    char str[1024];
    char ftime[20];
    time_t my_time = msg->stamp.toSec();
    strftime(ftime, 20, "%Y-%m-%d %H:%M:%S", localtime(&my_time));
    sprintf(str, "Time %s\nScore %d:%d \nComment %s\n", ftime, msg->team_one, msg->team_two, msg->comment.c_str());
    ROS_INFO_STREAM(str);
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "subscriber", ros::init_options::AnonymousName);
    ros::NodeHandle n;
    ros::Subscriber s = n.subscribe("topic", 5, callback);
    ros::spin();
    return 0;
}


