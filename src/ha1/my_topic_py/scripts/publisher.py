#!/usr/bin/env python
import rospy
import random
import time
from msgs.msg import Match

def publisher():
    rospy.init_node("publisher")
    p = rospy.Publisher("topic", Match, queue_size=5)
    rate = rospy.Rate(0.25)
    while not rospy.is_shutdown():
	score = Match()
        score.stamp = rospy.get_rostime()
        score.team_one = random.randint(1, 10)
        score.team_two = random.randint(1, 10)
        score.comment = "what a goal!"

        p.publish(score)
        rate.sleep()

if __name__ == '__main__':
    try:
        publisher()
    except rospy.ROSInterruptException:
        pass

