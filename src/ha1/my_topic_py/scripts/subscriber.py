#!/usr/bin/env python
import rospy
from msgs.msg import Match  
from datetime import datetime

def callback(msg):
    fmsg = "time is %s\nscore %d:%d\ncomment: %s\n" % (datetime.utcfromtimestamp(msg.stamp.to_sec()).strftime('%Y-%m-%d %H:%M:%S'), msg.team_one, msg.team_two, msg.comment) 
    rospy.loginfo(fmsg)
    
def subscriber():
    rospy.init_node("subscriber", anonymous=True)
    rospy.Subscriber("topic", Match, callback)
    rospy.spin()

if __name__ == '__main__':
    subscriber()


