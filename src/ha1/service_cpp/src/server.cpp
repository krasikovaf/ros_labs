#include <ros/ros.h>
#include <std_srvs/Empty.h>

bool callback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &resp)
{
    return true;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "server");
    ros::NodeHandle n;
    ros::ServiceServer s = n.advertiseService("service", callback);
    ros::spin();
    return 0;
}


