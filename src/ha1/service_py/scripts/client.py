#!/usr/bin/env python
import rospy
from std_srvs.srv import Empty

def client():
    rospy.wait_for_service("service")
    try:
        sp = rospy.ServiceProxy("service", Empty)
        resp = sp()
        print "response is so empty"
    except rospy.ServiceException, e:
        print "Service call failed: " + str(e)

if __name__ == "__main__":
    print "Calling service: without info"
    client()


