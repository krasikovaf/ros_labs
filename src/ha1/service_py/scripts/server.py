#!/usr/bin/env python
import rospy
from std_srvs.srv import Empty, EmptyResponse

def callback(req):
    rospy.loginfo("someone accessed me...\n")
    return EmptyResponse()

def server():
    rospy.init_node("server")
    s = rospy.Service("service", Empty, callback)
    rospy.spin()

if __name__ == "__main__":
    server()

