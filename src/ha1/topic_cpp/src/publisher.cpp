#include <ros/ros.h>
#include <stdlib.h>
#include <time.h>
#include <geometry_msgs/Pose.h>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "publisher");
    ros::NodeHandle n;
    ros::Publisher p = n.advertise<geometry_msgs::Pose>("topic", 5);
    ros::Rate rate(0.25);
    while (ros::ok())
    {
	srand(time(0));
        geometry_msgs::Pose msg;
        msg.position.x = rand();
        msg.position.y = rand();

        msg.orientation.x = rand();
        msg.orientation.y = rand();

        p.publish(msg);
        rate.sleep();
    }
    return 0;
}


