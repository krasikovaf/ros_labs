#include <ros/ros.h>
#include <stdio.h>
#include <geometry_msgs/Pose.h>

void callback(const geometry_msgs::Pose::ConstPtr& msg)
{
    char str[1024];
    sprintf(str, "Position\n-----\nx = %f\ny = %f\nOrientation\n-----\nx = %f\ny = %f\n", msg->position.x, msg->position.y, msg->orientation.x, msg->orientation.y);
    ROS_INFO_STREAM(str);
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "subscriber", ros::init_options::AnonymousName);
    ros::NodeHandle n;
    ros::Subscriber s = n.subscribe("topic", 5, callback);
    ros::spin();
    return 0;
}


