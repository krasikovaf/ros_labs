#!/usr/bin/env python
import rospy
import random
from geometry_msgs.msg import Pose

def publisher():
    rospy.init_node("publisher")
    p = rospy.Publisher("topic", Pose, queue_size=5)
    rate = rospy.Rate(0.25)
    while not rospy.is_shutdown():
	pose = Pose()
	pose.position.x = round(random.random(), 2) 
	pose.position.y = round(random.random(), 2)

	pose.orientation.x = round(random.random(), 2)
	pose.orientation.y = round(random.random(), 2)
        p.publish(pose)
        rate.sleep()

if __name__ == '__main__':
    try:
        publisher()
    except rospy.ROSInterruptException:
        pass

