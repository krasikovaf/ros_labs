#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Pose

def callback(msg):
    fmsg = "pose position\n-----\nx = %f\ny = %f\npose orientation\n------\nx = %f\ny = %f\n" % (msg.position.x, msg.position.y, msg.orientation.x, msg.orientation.y) 
    rospy.loginfo(fmsg)
    
def subscriber():
    rospy.init_node("subscriber", anonymous=True)
    rospy.Subscriber("topic", Pose, callback)
    rospy.spin()

if __name__ == '__main__':
    subscriber()


