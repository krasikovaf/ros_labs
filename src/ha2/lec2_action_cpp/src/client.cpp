#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <lec2_actions/MoveAction.h>

void cbDone(const actionlib::SimpleClientGoalState& state, const lec2_actions::MoveResultConstPtr& result)
{
    ROS_INFO_STREAM("DONE. State: " << state.toString());
    ROS_INFO_STREAM("result.position: " << result->nrounds);
    ros::shutdown();
}

void cbActive()
{
    ROS_INFO("The action is active now.");
}

void cbFeedback(const lec2_actions::MoveFeedbackConstPtr& feedback)
{
    ROS_INFO("FEEDBACK");
    ROS_INFO_STREAM("round: " << feedback->nround);
}

int main (int argc, char **argv)
{
    ros::init(argc, argv, "move_client", ros::init_options::AnonymousName);
    actionlib::SimpleActionClient<lec2_actions::MoveAction> sac("move", true);

    ROS_INFO("Waiting for the action server...");
    sac.waitForServer();

    ROS_INFO("Sending goal...");
    lec2_actions::MoveGoal goal;
    ros::NodeHandle nh;
    
    int rounds;
    int speed;

    nh.param("/move_server/rounds", rounds,1);
    nh.param("/move_server/speed", speed,1);
    
    goal.rounds = rounds;
    goal.speed = speed;

    sac.sendGoal(goal, &cbDone, &cbActive, &cbFeedback);

    ros::spin();

    return 0;
}

