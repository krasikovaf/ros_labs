#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <lec2_actions/MoveAction.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <dynamic_reconfigure/server.h>
#include <lec2_action_cpp/DRConfig.h>
#include <math.h>


#define PI 3.14159265 

class MoveAction
{
private:	
	float X=0.1,Y=0.2,Z=0.3,A=1.0,B=2.0,alpha=0.1; 
	int rounds=1;		
 	ros::NodeHandle nh;
	tf2_ros::TransformBroadcaster tb;
    	actionlib::SimpleActionServer<lec2_actions::MoveAction> sas;
	double rate_time = 30.0;

	void _move_and_pub()
	{
        	ros::Rate rate(rate_time);
		move_bit();
		publish_tf();
		publish_action();
		rate.sleep();
	}
public:
	MoveAction(std::string name) : sas(nh, name, boost::bind(&MoveAction::move_and_pub, this, _1), false)
	{								
		sas.start();
	}

	void move_and_pub(const lec2_actions::MoveGoalConstPtr &goal)
	{			
		dynamic_reconfigure::Server<lec2_action_cpp::DRConfig> dserver;
		dynamic_reconfigure::Server<lec2_action_cpp::DRConfig>::CallbackType cb;
		cb = boost::bind(&MoveAction::set_params, this, _1, _2);	
		dserver.setCallback(cb);
		rounds = goal->rounds;
		rate_time = goal->speed;
		while (get_nround() < rounds){
			if (sas.isPreemptRequested() || !ros::ok())
			{
				sas.setPreempted();
				return;
			}
			_move_and_pub();
		}

		publish_action_result();	
	}
	
	void set_params(lec2_action_cpp::DRConfig &config, uint32_t level)
	{
		rate_time = config.speed;
		rounds = config.rounds;		
	}

	void publish_tf()
	{
		geometry_msgs::TransformStamped ts;
		ts.header.stamp = ros::Time::now();
		ts.header.frame_id = "world";
		ts.child_frame_id = "base_link";
		ts.transform.translation.x = X;
		ts.transform.translation.y = Y;
		ts.transform.translation.z = Z;
		tf2::Quaternion q;
		q.setRPY(0, 0, 0);
		ts.transform.rotation.x = q.x();
		ts.transform.rotation.y = q.y();
		ts.transform.rotation.z = q.z();
		ts.transform.rotation.w = q.w();
		tb.sendTransform(ts);				
	}

	void move_bit()
	{
		X = A * cos(alpha); 
		Y = B * sin(alpha);
		alpha += 0.01 * PI;
	}		
	
	uint32_t get_nround()
	{
		return uint32_t(alpha / (2*PI));
	}
	
	void publish_action_result()
	{		
		lec2_actions::MoveResult result;
		result.nrounds = get_nround();
		sas.setSucceeded(result);	 
	}	

	void publish_action()
	{
		lec2_actions::MoveFeedback feedback;
		feedback.nround = get_nround();
		sas.publishFeedback(feedback);			
	}	
};

int main(int argc, char* argv[])
{
	ros::init(argc, argv, "move_server");
    	MoveAction move("move");
    	ros::spin();	
	return 0;
}
