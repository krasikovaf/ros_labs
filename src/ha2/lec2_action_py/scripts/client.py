#!/usr/bin/env python
import rospy
import actionlib
import lec2_actions.msg


def cb_done(state, result):
    rospy.loginfo("DONE. State: %s",
                  ["PENDING", "ACTIVE", "PREEMPTED",
                   "SUCCEEDED", "ABORTED", "REJECTED", "PREEMPTING",
                   "RECALLING", "RECALLED", "LOST"][state])
    rospy.loginfo("%s", str(result).replace('\n', '; '))
    rospy.signal_shutdown("quit")


def cb_active():
    rospy.loginfo("The action is active now.")


def cb_feedback(feedback):
    rospy.loginfo("FEEDBACK")
    rospy.loginfo("%s", str(feedback).replace('\n', '; '))


def client(rounds, speed):
    rospy.init_node('move_client', anonymous=True)
    sac = actionlib.SimpleActionClient('move', lec2_actions.msg.MoveAction)
    rospy.loginfo("Waiting for the action server...")
    sac.wait_for_server()
    goal = lec2_actions.msg.MoveGoal(rounds, speed)
    rospy.loginfo("Sending goal...")
    sac.send_goal(goal, cb_done, cb_active, cb_feedback)
    rospy.spin()


if __name__ == "__main__":
    rounds = rospy.get_param('/move_server/rounds')
    speed = rospy.get_param('/move_server/speed')
    rospy.loginfo(speed)
    client(rounds, speed)
