#!/usr/bin/env python
import rospy
import actionlib
import lec2_actions.msg
import tf_conversions
import tf2_ros
import geometry_msgs.msg
import math
from dynamic_reconfigure.server import Server
from lec2_action_py.cfg import DRConfig


class MoveAction(object):

    def __init__(self, name):
        self.X = 0.1
        self.Y = 0.2
        self.Z = 0.3
        self.A = 1.0
        self.B = 2.0
        self.alpha = 0.0
        self.rate_time = 30.0
        self.rounds = 1
        self.sas = actionlib.SimpleActionServer(name,
                                                lec2_actions.msg.MoveAction,
                                                execute_cb=self.move_and_pub,
                                                auto_start=False)
        self.rate = rospy.Rate(self.rate_time)
        self.srv = Server(DRConfig, self.config_cb)
        self.tb = tf2_ros.TransformBroadcaster()

    def set_rate_time(self, rate_time):
        self.rate_time = rate_time
        self.rate = rospy.Rate(self.rate_time)

    def config_cb(self, config, level):
        self.set_params(config)
        return config

    def set_params(self, config):
        self.set_rate_time(config['speed'])
        self.rounds = config['rounds']

    def publish_tf(self):
        ts = geometry_msgs.msg.TransformStamped()
        ts.header.stamp = rospy.Time.now()
        ts.header.frame_id = "world"
        ts.child_frame_id = "base_link"
        ts.transform.translation.x = self.X
        ts.transform.translation.y = self.Y
        ts.transform.translation.z = self.Z
        q = tf_conversions.transformations.quaternion_from_euler(0, 0, 0)
        ts.transform.rotation.x = q[0]
        ts.transform.rotation.y = q[1]
        ts.transform.rotation.z = q[2]
        ts.transform.rotation.w = q[3]
        self.tb.sendTransform(ts)

    def move_and_pub(self, goal):
        self.set_rate_time(goal.speed)
        self.rounds = goal.rounds
        if self.rounds == 0:
            while not rospy.is_shutdown():
                self._move_and_pub()
        else:
            while self.get_nround() < self.rounds:
                self._move_and_pub()

        self.publish_action_result()

    def get_nround(self):
        return self.alpha / (2*math.pi)

    def _move_and_pub(self):
        self.move_bit()
        self.publish_tf()
        self.publish_action()
        self.rate.sleep()

    def publish_action_result(self):
        result = lec2_actions.msg.MoveResult()
        result.nrounds = self.get_nround()
        self.sas.set_succeeded(result)

    def publish_action(self):
        feedback = lec2_actions.msg.MoveFeedback()
        feedback.nround = self.get_nround()
        self.sas.publish_feedback(feedback)

    def move_bit(self):
        self.X = self.A * math.cos(self.alpha)
        self.Y = self.B * math.sin(self.alpha)
        self.alpha += 0.01 * math.pi


if __name__ == '__main__':
    rospy.init_node('move_server')
    move = MoveAction('move')
    move.sas.start()
    rospy.spin()
